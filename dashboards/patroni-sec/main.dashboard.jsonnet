local panels = import 'gitlab-dashboards/patroni-panels.libsonnet';

panels.patroni('patroni-sec', 'gitlab-sec', useTimeSeriesPlugin=true)
