<!-- MARKER: do not edit this section directly. Edit services/service-catalog.yml then run scripts/generate-docs -->

**Table of Contents**

[[_TOC_]]

# Amp Service

* **Alerts**: <https://alerts.gitlab.net/#/alerts?filter=%7Btype%3D%22amp%22%2C%20tier%3D%22inf%22%7D>
* **Label**: gitlab-com/gl-infra/production~"Service::Amp"

## Logging

* [cellsdev](https://console.cloud.google.com/logs/query?project=amp-b6f1)
* [cellsprod](https://console.cloud.google.com/logs/query?project=amp-3c0d)

## Troubleshooting Pointers

* [Cells and Amp Documentation](../cells/amp.md)
* [Breakglass](../cells/breakglass.md)
* [Cells DNS](../cells/dns.md)
<!-- END_MARKER -->

<!-- ## Summary -->

<!-- ## Architecture -->

<!-- ## Performance -->

<!-- ## Scalability -->

<!-- ## Availability -->

<!-- ## Durability -->

<!-- ## Security/Compliance -->

<!-- ## Monitoring/Alerting -->

<!-- ## Links to further Documentation -->
