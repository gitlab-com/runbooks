<!-- MARKER: do not edit this section directly. Edit services/service-catalog.yml then run scripts/generate-docs -->

**Table of Contents**

[[_TOC_]]

# Example Runway-managed Postgres Service

* [Service Overview](https://dashboards.gitlab.net/d/runway-db-example-main/runway-db-example-overview)
* **Alerts**: <https://alerts.gitlab.net/#/alerts?filter=%7Btype%3D%22runway-db-example%22%2C%20tier%3D%22db%22%7D>
* **Label**: gitlab-com/gl-infra/production~"Service::RunwayPostgresExample"

## Logging

* [gcp](https://cloudlogging.app.goo.gl/yUkd8JGYdPJUcNyq9)

## Troubleshooting Pointers

* [Cloud SQL Restore Pipeline Troubleshooting](../runway/cloudsql_restore_pipeline_troubleshooting.md)
<!-- END_MARKER -->

<!-- ## Summary -->

<!-- ## Architecture -->

<!-- ## Performance -->

<!-- ## Scalability -->

<!-- ## Availability -->

<!-- ## Durability -->

<!-- ## Security/Compliance -->

<!-- ## Monitoring/Alerting -->

<!-- ## Links to further Documentation -->
