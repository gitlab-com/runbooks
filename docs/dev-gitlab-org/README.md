<!-- MARKER: do not edit this section directly. Edit services/service-catalog.yml then run scripts/generate-docs -->

**Table of Contents**

[[_TOC_]]

# dev.gitlab.org Service

* **Alerts**: <https://alerts.gitlab.net/#/alerts?filter=%7Btype%3D%22dev-gitlab-org%22%2C%20tier%3D%22sv%22%7D>
* **Label**: gitlab-com/gl-infra/production~"Service::DevGitlabOrg"

## Logging

* [Rails](https://nonprod-log.gitlab.net/app/r/s/FUSMg)
* [Gitaly](https://nonprod-log.gitlab.net/app/r/s/DUsV1)
* [GitLab Shell](https://nonprod-log.gitlab.net/app/r/s/nDLCx)
* [Sidekiq](https://nonprod-log.gitlab.net/app/r/s/k0z53)

## Troubleshooting Pointers

* [dev.gitlab.org host](../bastions/dev-host.md)
<!-- END_MARKER -->

<!-- ## Summary -->

<!-- ## Architecture -->

<!-- ## Performance -->

<!-- ## Scalability -->

<!-- ## Availability -->

<!-- ## Durability -->

<!-- ## Security/Compliance -->

<!-- ## Monitoring/Alerting -->

<!-- ## Links to further Documentation -->
