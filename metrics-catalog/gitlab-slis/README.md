# GitLab Service Level Indicators

## User Journeys

You can find details about **User Journeys** and **Service Level Indicators (SLIs)** on the [Observability Docs Hub](https://gitlab-com.gitlab.io/gl-infra/observability/docs-hub/user-journeys-and-slis/).
