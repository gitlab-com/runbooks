{
  errorLevel(value)::
    self {
      value: value,
      color: 'rgba(242, 73, 92, 0.5)',
    },
  warningLevel(value)::
    self {
      value: value,
      color: 'rgba(242, 73, 92, 0.25)',
    },
  optimalLevel(value)::
    self {
      value: value,
      color: 'rgba(86, 166, 75, 0.25)',
    },
}
